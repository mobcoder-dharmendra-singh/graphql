import React from "react";
import { useQuery, gql, useMutation } from "@apollo/client";

const FILMS_QUERY = gql`
{
  games {
  platform,
  title,
  id,
  
},
reviewes{
  rating,
  content,
  id,
  game{
    title,
    platform
  },
  auther{
    name 
  }
},
authers{
  id,
  name,
  verified
}
}
`



const mutaionData = gql`
mutation deleteGame($deleteId: ID!) {
  deleteGame(id: $deleteId) {
    id,
  }
  deleteAuther(id :$deleteId){
    id,
  }
  deleteReview(id:$deleteId){
    id,
  }
}
`

const addGameMutation = gql`
mutation addGame($item:AddGameInput!){
  addGame(game: $item) {
    id,
    title
    platform
  }
}
`
const editGameMutation = gql`
mutation editGame($item:EditGameInput!,$id: ID!){
  updateGame(edits: $item,id: $id){
    id,
    title
    platform
  }
}
`

const addAutherMutation = gql`
mutation addAuther($item:AddAutherInput!){
addAuther(auther: $item){
    name,
    verified
  }
}
`

const editAutherMutation = gql`
mutation editAuther($item:EditAutherInput!,$id: ID!){
  updateAuther(edits: $item,id: $id){
    name,
    verified
  }

}
`

const addReviewMutation = gql`
mutation addReview($item:AddReviewInput!){
addReview(review: $item){
    rating,
    content
  }
}
`
const editReviewMutation = gql`
mutation editReview($item:EditReviewInput!,$id: ID!){
  updateReview(edits: $item,id: $id){
    rating,
    content
  }
}
`

export const App = () => {
  const { data, loading, error, refetch } = useQuery(FILMS_QUERY);
  const [deleteGame] = useMutation(mutaionData)
  const [addGame] = useMutation(addGameMutation)
  const [addAuther] = useMutation(addAutherMutation)
  const [addReview] = useMutation(addReviewMutation)
  const [editGame] = useMutation(editGameMutation)
  const [editAuther] = useMutation(editAutherMutation)
  const [editReview] = useMutation(editReviewMutation)
 


  if (loading) return "Loading...";
  if (error) return <pre>{error.message}</pre>

  const onClick = (id) => {
    deleteGame({ variables: { deleteId: id } });
    refetch()
  }

  const game = {
    "title": "new game added",
    "platform": ["string", "platform", "new"]
  }

  const auther = {
    "name": "Abhinav Chaurasiya",
    "verified": true
  }
  const review ={
    "rating": 9,
    "content":'review added by '
  }

  const onAddGame = () => {
    addGame({ variables: { item: game } })
    refetch()
  }
  const onAddAuther = () => {
    addAuther({ variables: { item: auther } })
    refetch()
  }

  const onAddReview =()=>{
    addReview({variables:{item:review}})
    refetch()
  }

  const onEditGame =(id)=>{
editGame({variables:{item:game,id:id}})
refetch()
  }

  const onEditAuther =(id)=>{
editAuther({variables:{item:auther,id:id}})
refetch()
  }

  const onEditReview =(id)=>{
editReview({variables:{item:review,id:id}})
refetch()

  }


  return (
    <div>
      <h1>Game Details</h1>
      <button onClick={() => onAddGame()}>Add</button>
      <ul>
        {data.games.map((game, index) => (
          <div key={index}><li key={index}>Name:{game?.title}  Type: {game?.id}</li><div>
            <button onClick={() => onClick(game?.id)}>Delete</button>
            <button onClick={() => onEditGame(game?.id)}>Edit</button>
          </div></div>
        ))}
      </ul>
      <h1>Review Details</h1>
      <button onClick={()=> onAddReview()}>Add</button>
      <ul>
        {data.reviewes.map((review, index) => (
          <div key={index}>
            <li key={index}>Rating:{review?.rating}  Comment: {review?.content}</li>
            <button onClick={() => onClick(review?.id)}>Delete</button>
            <button onClick={() => onEditReview(review?.id)}>Edit</button>
          </div>
        ))}
      </ul>
      <h1>Auther Details</h1>
      <button onClick={() => onAddAuther()} >Add</button>
      <ul>
        {data.authers.map((auther, index) => {
          let Status = auther?.verified
          console.log(Status,'status')
          return (
            <div key={index}>
              <li >Rating:{auther?.name}  Status: {Status ? "Verified" : "Unverified"}</li>
              <button onClick={() => onClick(auther?.id)}>Delete</button>
              <button onClick={() => onEditAuther(auther?.id)}>Edit</button>
            </div>
          )

        })}
      </ul>
    </div>
  );
}